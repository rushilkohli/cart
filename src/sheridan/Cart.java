package sheridan;
import java.util.ArrayList;
public class Cart {
private final ArrayList<Product> products;
private PaymentService service;
public Cart() {
products = new ArrayList<>();
}
public void setPaymentService(PaymentService service) {
this.service = service;
}
public void addProduct(Product product) {
products.add(product);
}
public void payCart() {
double totalPrice = 0;
totalPrice = products.stream().map(p -> p.getPrice()).reduce(totalPrice, (accumulator, _item) -> accumulator + _item);
service.processPayment(totalPrice);
}
}
